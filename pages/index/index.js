//获取应用实例
const app = getApp()
var CON = require('../../utils/constant.js');
var NET = require('../../utils/netUtil.js');
var UTIL = require('../../utils/util.js');

Page({
  data: {
    registered: false,
    filter: 0,
    propertyList: [],
    //map set
    latitude: -37.8182668, //纬度
    longitude: 144.9648731, //经度
    index: 0,
    tabs: ['th', 'apt', 'com'],
    markers: [],
    points: [],
    scale: 12,
    displayDetail: false,
    displayPicker: false,
    controls: [],
    windowHeight: 400,
    windowWidth: 200,
    statusColor: {
      'new': '#1EBDB2',
      'update': '#e4002b',
      'sold': '#ffb200'
    },
    langs: Object,
  },

  onLoad: function() {
    let self = this;
    wx.showLoading({title: 'Loading'});
    this.loadProperties();
  },

  loadProperties: function() {
    this.setData({ langs: app.globalData.langs, registered: wx.getStorageSync("registered")});
    wx.setNavigationBarTitle({
      title: this.data.langs['index.title']
    })
    NET.netUtil(CON.PROPERTY_URL, 'GET', '', ret => {
      if (ret) {
        let myFav = wx.getStorageSync("myFav");
        let tmp = [];
        ret.forEach(p => {
          p.fav = myFav[p.propertyId];
          tmp.push(p);
        })
        wx.setStorageSync("propertyList", tmp);
        this.filterPropertyList("th");
      }
      wx.hideLoading();
    });
  },

  onShow: function(e) {
    if (wx.getStorageSync("refreshIndexPropertyList")) {
      if (this.data.filter == 0) {
        this.filterPropertyList("th");
      } else {
        this.filterPropertyList("apt");
      }
      wx.setStorageSync("refreshIndexPropertyList", false); //无须主动刷新,使用缓存orderlist
    }
  },

  filterPropertyList: function(category) {
    let propertyList = wx.getStorageSync("propertyList")
    let filterd = propertyList.filter(p => {
      return p.category && p.category.includes(category);
    });

    this.setData({
      propertyList: filterd
    });
  },

  clickDetail: function(e) {
    let idx = e.currentTarget.dataset.propertyid;
    wx.navigateTo({
      url: '/pages/detail/detail?propertyId=' + idx,
    })
  },

  searchByAddress: function(e) {
    let tmp = {};
    let query = e.detail.value,
      propertyList = wx.getStorageSync("propertyList");
    for (let i in propertyList) {
      if (propertyList[i].address.toLowerCase().indexOf(query.toLowerCase()) > -1 && propertyList[i].category == this.data.tabs[this.data.filter]) {
        tmp[i] = propertyList[i]
      }
    }
    this.setData({
      search: query,
      displayClear: true,
      propertyList: tmp
    });
  },

  resetSearch: function(e) {
    this.setData({
      search: '',
      displayClear: false
    });
    this.resetDisplay();
  },

  resetDisplay: function(e) {
    this.filterPropertyList(this.data.tabs[this.data.filter]);
  },

  clickTH: function(e) {
    this.setData({
      filter: 0
    });
    this.filterPropertyList("th");
  },

  clickAPT: function(e) {
    this.setData({
      filter: 1
    });
    this.filterPropertyList("apt");
  },

  clickCOM: function(e) {
    this.setData({
      filter: 2
    });
    this.filterPropertyList("com");
  },

  getMarkers(markerList) {
    let markers = [];
    for (let item of markerList) {
      let marker = this.createMarker(item);
      markers.push(marker);
    }
    return markers;
  },

  createMarker(point) {
    let latitude = point.latitude;
    let longitude = point.longitude;
    let marker = {
      iconPath: "/images/dot.png",
      id: point.propertyId,
      address: point.address,
      room: point.room,
      toilet: point.toilet,
      garage: point.garage,
      price: point.price,
      fav: point.fav,
      latitude: latitude,
      longitude: longitude,
      width: 25,
      height: 25
    };
    return marker;
  },

  onMarkerClick: function(e) {
    var markerId = e.markerId,
      markers = this.data.markers,
      index = 0;
    for (var i = 0; i < markers.length; i++) {
      markers[i].width = 25;
      markers[i].height = 25;
      markers[i].iconPath = "/images/dot.png";
      if (markers[i].id == markerId) {
        markers[i].width = 28;
        markers[i].height = 28;
        markers[i].iconPath = "/images/marker.png",
          markers[i].iconPath = "/images/marker.png",
          index = i;
      }
    }
    this.setData({
      markers: markers,
      index: index,
      displayDetail: true
    });
  },

  onMapClick: function(e) {
    this.setData({
      displayDetail: false
    });
  },

  setControlls: function(windowWidth) {
    this.setData({
      controls: [{
          id: 2,
          iconPath: '/images/location.png',
          position: {
            left: windowWidth - 40,
            top: 100 - 50,
            width: 28,
            height: 28
          },
          clickable: true
        },
        {
          id: 1,
          iconPath: '/images/layers.png',
          position: {
            left: windowWidth - 40,
            top: 100,
            width: 28,
            height: 28
          },
          clickable: true
        },
      ]
    })
  },

  controltap: function(e) {
    let self = this;
    if (e.controlId === 1) {
      self.setData({
        displayPicker: true
      });

    } else if (e.controlId === 2) {
      wx.getLocation({
        //type: 'wgs84',
        type: 'gcj02',
        success: function(res) {
          self.setData({
            latitude: res.latitude,
            longitude: res.longitude,
            scale: 12
          });
        },
        fail: function(res) {
          wx.showToast({
            title: '定位失败',
            duration: 1800
          })
        }
      })
    }
  },

  saveFav: function(e) {
    let propertyId = e.currentTarget.dataset.property;
    UTIL.switchFav(propertyId);
    if (this.data.filter == 0) {
      this.filterPropertyList("th");
    } else {
      this.filterPropertyList("apt");
    }
    //this.setData({ propertyList: wx.getStorageSync("propertyList") });
  },

  goToReg: function(e) {
    wx.navigateTo({
      url: '/pages/register/register',
    })
  },

})