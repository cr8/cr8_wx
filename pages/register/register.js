const app = getApp()
var CON = require('../../utils/constant.js');
var NetUtil = require('../../utils/netUtil.js');
var Util = require('../../utils/util.js');

var interval;
Page({

  /**
   * 页面的初始数据
   */
  data: {
    array: ["🇦🇺","🇨🇳","✉️"],
    index:0,
    inputType: ["number","number","text"],
    mobile: "",
    code: "",
    ctd: "",
    btnTxt: "",
    disable: false,
    allowToTry: 5,
    user: {},
    placeHolders: [],
    langs: Object,
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    this.setData({ langs: app.globalData.langs,});
    let placeHolders = [this.data.langs['reg.au.mobile'], this.data.langs['reg.cn.mobile'], this.data.langs['reg.email']];
    
    this.setData({ placeHolders: placeHolders, btnTxt: this.data.langs['reg.send.code']});
    let user = wx.getStorageSync('userInfo');
    if (user) {
      if (user.blockedSMS) {
        if (Date.parse(user.blockedSMS) < Date.parse(new Date())) {
          user.allowToTry = 3;
        }
      }
      let allwoToTry = user.allowToTry == undefined ? 5 : user.allowToTry;
      this.setData({ user: user, allowToTry: allwoToTry });
    }
    wx.setNavigationBarTitle({
      title: this.data.langs['reg.title']
    })
  },

  bindPickerChange: function (e) {
    this.setData({index: e.detail.value});
    

  },

  sendMsg: function (event) {
    let mobile = this.data.mobile;
    if (!this.validateMobile(event, mobile)) return;
    if (this.data.allowToTry > 0) {
      let self = this, user = this.data.user;
      if (!this.data.disable && this.data.allowToTry > 0 && this.data.allowToTry <= 5) {
        let allowToTry = --this.data.allowToTry;
        this.setData({ allowToTry: allowToTry });
        //call api to send code;
        NetUtil.netUtil(CON.SMS_URL + this.data.index + "/"+mobile+"/", "GET", "", function (res) {

        })
      }

      if (this.data.allowToTry == 0) {
        let d = new Date();
        d.setHours(d.getHours() + 24);
        //d.setMinutes(d.getMinutes() + 1);
        user.blockedSMS = d;
      }
      user.allowToTry = this.data.allowToTry;
      wx.setStorage({
        key: "userInfo",
        data: user,
      })
    }
    
      this.countDown();
    
  },

  verifyCode: function (e) {
    if (!this.validateMobile(e, this.data.mobile)) return;
    if (!this.validdateCode(e, e.detail.value.code)) return;

    if (this.data.allowToTry > 0) {
      let mobile = this.data.mobile;
      let code = e.detail.value.code;
 
      NetUtil.netUtil(CON.SMS_URL + mobile + "/code/" + code, "GET", "", function (res) {
        if (res) {
          let user = { contact:mobile};
          NetUtil.netUtil(CON.USER_URL + "/save", "POST", user, function (res) {});
          wx.setStorageSync("contact", mobile);
          wx.setStorageSync("registered", true);

          wx.showToast({
            title: 'Success',
            icon: 'success',
            duration: 1200,
            success: function () {
              var pages = getCurrentPages();
              if (pages.length > 1) {
                var prevPage = pages[pages.length - 2];
                prevPage.setData({
                  registered: true
                })
              }
              wx.navigateBack({});
            }
          })
        } else {
          let self = this;
          wx.showModal({
            title: self.data.langs['reg.warning'],
            content: self.data.langs['reg.code.error'],
            showCancel: false,
          })
        }

      })
    }
  },

  inputMobile: function (e) {
    this.setData({ mobile: e.detail.value });
  },

  validateMobile: function (e, mobile) {
    if (
      ((this.data.index == 0 && mobile.length == 10 && mobile.startsWith("04") || 
        this.data.index == 1 && mobile.length == 11 && mobile.startsWith("1")) && Util.isNumeric(mobile)) 
        ||(this.data.index == 2 && mobile.indexOf("@")>0)
     ) {
      this.setData({ mobile: mobile });
      return true;
    } else {
      let self = this;
      wx.showModal({
        title: self.data.langs['reg.warning'],
        content: self.data.langs['reg.check.input'],
        showCancel: false,
      })
      return false;
    }
  },

  validdateCode: function (e, code) {
    if (code.length == 4 && Util.isNumeric(code)) {
      return true;
    } else {
      let self = this;
      wx.showModal({
        title: self.data.langs['reg.warning'],
        content: self.data.langs['reg.code.error'],
        showCancel: false,
      })
      return false;
    }
  },


  countDown: function () {
    let self = this;
    this.setData({ ctd: '90', disable: true, btnTxt: 's ' + self.data.langs['reg.resend'] });
    let countDown = 90;
    interval = setInterval(function () {
      countDown--;
      self.setData({ ctd: countDown });
      if (countDown == 0) {
        clearInterval(interval);
        
        self.setData({ disable: false, ctd: '', btnTxt: self.data.langs['reg.send.code'] });
      }
    }, 1000);
  },

  /**
     * 生命周期函数--监听页面卸载
     */
  onUnload: function () {
    clearInterval(interval);
  },


  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})