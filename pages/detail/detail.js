const app = getApp()
var CON = require('../../utils/constant.js');
var NET = require('../../utils/netUtil.js');
var UTIL = require('../../utils/util.js');

Page({

  /**
   * 页面的初始数据
   */
  data: {
    registered: false,
    currentId: 0,
    features: [],
    property: Object,
    schools: [],
    desc:'',
    descReadMore: 'block',
    price: '',
    brochure: '',
    more: '',
    favIcon: false,
    markers: [],
    shared: false,
    langs: Object,
    docUrl: '',
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function(options) {
    let currentId = options.propertyId,
      self = this,
      shared = options.shared;
    self.setData({
      langs: app.globalData.langs,
      currentId: currentId,
      shared: shared,
      registered: wx.getStorageSync("registered")
    });
    wx.setNavigationBarTitle({
      title: self.data.langs['detail.title']
    });
    let propertyList = wx.getStorageSync("propertyList");
    if (!propertyList) {
      NET.netUtil(CON.PROPERTY_URL + 'all', 'GET', '', ret => {
        if (ret) {
          self.setData({
            propertyList: ret
          });
          wx.setStorageSync("propertyList", ret);
          this.renderProperty();
        }
      });
    } else {
      this.renderProperty();
    }
  },

  renderProperty: function() {
    let propertyList = wx.getStorageSync("propertyList");
    let properties = propertyList.filter(p => p.id == this.data.currentId);
    if (properties[0]) {
      this.setData({ schools: properties[0].schools.slice(0, 2)});
      this.setData({ desc: properties[0].desc.substring(0, 300) + '...'});
      this.setData({property: properties[0]});
      
      let myFav = wx.getStorageSync("myFav");
      this.setData({
        // features: tmp,
        favIcon: myFav[this.data.property.id]
      });
      this.mapMarker();
    } else {
      this.backToHome();
    }
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function() {

  },

  // loadPicsAndFeatures: function(propertyId) {
  //   let url = CON.ALI_OSS_URL + propertyId;
  //   NET.netUtil(url, 'GET', '', ret => {
  //     if (ret) {
  //       let tmp = [];
  //       ret.forEach(f => {
  //         if (f.indexOf("jpg") != -1) {
  //           tmp.push(CON.IMAGE_URL + f);
  //         } else if (f.indexOf("price") > 0) {
  //           this.setData({
  //             price: f
  //           });
  //         } else if (f.indexOf("brochure") > 0) {
  //           this.setData({
  //             brochure: f
  //           });
  //         } else if (f.indexOf("more") > 0) {
  //           this.setData({
  //             more: f
  //           });
  //         }
  //       });
  //       this.setData({
  //         imgUrls: tmp
  //       });
  //     }
  //   });
  // },

  mapMarker: function() {
    let markers = [];
    let marker = {
      iconPath: "/images/marker.png",
      latitude: this.data.property.lat,
      longitude: this.data.property.lng,
      width: 28,
      height: 28
    };
    markers.push(marker);
    this.setData({
      markers: markers
    });
  },

  openFile: function(e) {
    let self = this;
    wx.showActionSheet({
      itemList: [self.data.langs['detail.open.file']],
      success: function(res) {
        let file = e.currentTarget.dataset.file;
        if (file) {
          wx.showLoading({
            title: "0%",
          })
          const downloadTask = wx.downloadFile({
            url: file,
            success: function(ret) {
              var filePath = ret.tempFilePath;
              if (res.tapIndex == 0) {
                wx.openDocument({
                  filePath: filePath,
                  // fileType: 'pdf',
                  success: function (res) {
                    console.log('打开文档成功');
                  },
                  fail: function (res) {
                    console.log('openDocument fail')
                    console.log(res)
                  },
                });
              } else {
                wx.openDocument({
                  filePath: filePath,
                  // fileType: 'pdf'
                });
              }
            }
          })
          downloadTask.onProgressUpdate((res) => {
            wx.showLoading({
              title: res.progress + "%",
            })
            if (res.progress == 100) wx.hideLoading();
          })
        } else {
          wx.showToast({
            title: 'Empty file!!!'
          });
        }
      }
    })
  },

  saveFile(file) {
    wx.saveFile({
      tempFilePath: this.data.imageSrc,
      success: function (res) {
        var savedFilePath = res.savedFilePath
        console.log("savedFilePath", savedFilePath)
      }
    })
  },

  gotoFile: function(e) {
    let docUrl = e.currentTarget.dataset.file;
    wx.navigateTo({
      url: '/pages/detail/pdf?docUrl=' + docUrl,
    })
  },

  zoomInImg: function(e) {
    var self = this;
    var src = e.currentTarget.dataset.src.url;
    var urls = self.data.property.images.map(image=>image.url);
    wx.previewImage({
      current: src,
      urls: urls,
    })
  },

  saveFav: function(e) {
    let propertyId = this.data.property.id;
    UTIL.switchFav(propertyId);
    this.setData({
      favIcon: wx.getStorageSync("myFav")[propertyId]
    });
  },

  expandDesc: function(e) {
    this.setData({ desc: this.data.property.desc, descReadMore:'none' });
  },

  expandSchool: function(e) {
    if(this.data.schools.length == this.data.property.schools.length){
      this.setData({ schools: this.data.property.schools.slice(0,2) });
    } else {
      this.setData({ schools: this.data.property.schools });
    }
  },

  backToHome: function(e) {
    wx.switchTab({
      url: '/pages/index/index'
    })
  },

  goToReg: function(e) {
    wx.navigateTo({
      url: '/pages/register/register',
    })
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function() {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function() {
    return {
      title: this.data.property.address,
      path: '/pages/detail/detail?propertyId=' + this.data.property.id + '&shared=true',
    }
  }
})