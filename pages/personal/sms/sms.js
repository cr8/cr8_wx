var CON = require('../../../utils/constant.js');
var NetUtil = require('../../../utils/netUtil.js');
var Util = require('../../../utils/util.js');

var interval;
Page({

  /**
   * 页面的初始数据
   */
  data: {
    mobile: "04",
    code: "",
    ctd: "",
    btnTxt: "Send Pin",
    disable: false,
    allowToTry: 5,
    user: {},
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    let user = wx.getStorageSync('userInfo');
    if (user) {
      if (user.blockedSMS) {
        if (Date.parse(user.blockedSMS) < Date.parse(new Date())) {
          user.allowToTry = 3;
        }
      }
      let allwoToTry = user.allowToTry == undefined ? 5 : user.allowToTry;
      this.setData({ user: user, allowToTry: allwoToTry });
    }
    wx.setNavigationBarTitle({
      title: 'Verification'
    })
  },

  sendMsg: function (event) {
    let mobile = this.data.mobile;
    if(!this.validdateMobile(event, mobile)) return;
    if (this.data.allowToTry > 0 && mobile.length == 10 && mobile.startsWith("04")) {
      let self = this, user = this.data.user;
      if (!this.data.disable && this.data.allowToTry > 0 && this.data.allowToTry <= 5) {
        let allowToTry = --this.data.allowToTry;
        this.setData({allowToTry: allowToTry}); 
        //call api to send code;
        NetUtil.netUtil(CON.SMS_URL + mobile, "GET", "", function (res) {

        })
      } 
      
      if (this.data.allowToTry == 0) {
        let d = new Date();
        d.setHours(d.getHours() + 24);
        //d.setMinutes(d.getMinutes() + 1);
        user.blockedSMS = d;
      }
      user.allowToTry = this.data.allowToTry;
      wx.setStorage({
        key: "userInfo",
        data: user,
      })
    }
    if (mobile.length == 10 && mobile.startsWith("04")) {
      this.countDown();
    }
  },

  verifyCode: function (e) {
    if (!this.validdateMobile(e, this.data.mobile)) return;
    if (!this.validdateCode(e, e.detail.value.code)) return;
    
    if (this.data.allowToTry > 0) {
      console.info('verify....');
      let mobile = this.data.mobile;
      let code = e.detail.value.code;
      let user = this.data.user;

      NetUtil.netUtil(CON.SMS_URL + mobile +"/code/"+code, "GET", "", function (res) {
        if (res) {
          user.verified = true;
          user.verifiedMobile = mobile;
          wx.setStorage({
            key: "userInfo",
            data: user,
            success: function () {
              wx.showToast({
                title: 'Success',
                icon: 'success',
                duration: 1200,
                success:function() {
                  wx.redirectTo({
                    url: '/pages/personal/property/list',
                  })
                }
              })
            }
          })
        } else {
          wx.showToast({
            title: 'Pin is wrong',
            duration: 1200
          })
        }

      })
    }
  },

  inputMobile: function(e) {
    this.setData({ mobile: e.detail.value });
  },

  validdateMobile: function(e, mobile) {
    if (mobile.length == 10 && mobile.startsWith("04") && Util.isNumeric(mobile)) {
      this.setData({ mobile: mobile });
      return true;
    } else {
      wx.showModal({
        title: 'Warning',
        content: 'Check your mobile number',
        showCancel: false,
      })
      return false;
    }
  },

  validdateCode: function (e, code) {
    if (code.length == 6 && Util.isNumeric(code)) {
      return true;
    } else {
      wx.showModal({
        title: 'Warning',
        content: 'Pin is wrong',
        showCancel: false,
      })
      return false;
    }
  },


  countDown: function() {
    let self = this;
    this.setData({ ctd: '90', disable: true, btnTxt: 's Resend' });
    let countDown = 90;
    interval = setInterval(function () {
      countDown--;
      self.setData({ ctd: countDown });
      if (countDown == 0) {
        clearInterval(interval);
        self.setData({ disable: false, ctd: '', btnTxt: 'Send Pin' });
      }
    }, 1000);
  },

  /**
     * 生命周期函数--监听页面卸载
     */
  onUnload: function () {
    clearInterval(interval);
  },


  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})