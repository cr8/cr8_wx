// pages/personal/about/us.js
const app = getApp();

Page({

  /**
   * 页面的初始数据
   */
  data: {
    navTab: ["ABOUT US", "CONTACT US"],
    currentNavtab: "0",
    markers: [],
    latitude: '-37.7965221',
    longitude: '145.0575636',
    width: 200,
    langs: Object,
  },

  switchTab: function (e) {
    this.setData({
      currentNavtab: e.currentTarget.dataset.idx,
    });
  },


  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    this.setData({ langs: app.globalData.langs, width: app.globalData.windowWidth - 30 })
    let navTab = [this.data.langs['personal.about'], this.data.langs['personal.contact']];
    this.setData({ navTab: navTab});
    this.mapMarker();
    wx.setNavigationBarTitle({
      title: this.data.langs['personal.title']
    })
  },

  mapMarker: function () {
    let markers = [];
    let marker = {
      iconPath: "/images/cr8-marker.png",
      latitude: this.data.latitude,
      longitude: this.data.longitude,
      width: 29,
      height: 45
    };
    markers.push(marker);
    this.setData({ markers: markers });
  },

  makeCall: function (e) {
    let number = e.currentTarget.dataset.mobile;
    wx.makePhoneCall({ phoneNumber: number })
  },

  goAdmin: function (e) {
    let userInfo = wx.getStorageSync("userInfo");
    if (userInfo) {
      if (userInfo.verified) {
        wx.navigateTo({
          url: '/pages/personal/property/list',
        })
        return;
      }
    }
    wx.navigateTo({
      url: '/pages/personal/sms/sms',
    })

  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})