const app = getApp();
var UTIL = require('../../../utils/util.js');

Page({

  /**
   * 页面的初始数据
   */
  data: {
    myProperties:[],
    statusColor: { 'new': '#1EBDB2', 'update': '#e4002b', 'sold': '#ffb200' },
    langs: Object,
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    this.setData({langs: app.globalData.langs});
    wx.setNavigationBarTitle({
      title: this.data.langs['collection.title']
    })
  },

  clickDetail: function (e) {
    let idx = e.currentTarget.dataset.propertyid;
    wx.navigateTo({
      url: '/pages/detail/detail?propertyId=' + idx,
    })
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {
  
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    this.listFavorate();
  },

  saveFav: function (e) {
    let propertyId = e.currentTarget.dataset.property;
    UTIL.switchFav(propertyId);
    this.listFavorate();
  },

  listFavorate: function() {
    let propertyList = wx.getStorageSync("propertyList");
    let myFav = wx.getStorageSync("myFav");
    let tmp = [];
    propertyList.forEach(p => {
      p.fav = myFav[p.id];
      if (p.fav) tmp.push(p);
    })

    this.setData({ myProperties: tmp });
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {
  
  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {
  
  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {
  
  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {
  
  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {
  
  }
})