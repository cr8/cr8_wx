//url相关
const formatTime = date => {
  const year = date.getFullYear()
  const month = date.getMonth() + 1
  const day = date.getDate()
  const hour = date.getHours()
  const minute = date.getMinutes()
  const second = date.getSeconds()

  return [year, month, day].map(formatNumber).join('/') + ' ' + [hour, minute, second].map(formatNumber).join(':')
}

const formatNumber = n => {
  n = n.toString()
  return n[1] ? n : '0' + n
}

function makeCall(number) {
  let valid = false;
  if ((number.startsWith("04") || number.startsWith("02") || number.startsWith("03") || number.startsWith("07") || number.startsWith("08")) && number.length == 10) {
    number = number;
    valid = true;
  } else if (number.startsWith("1") || number.startsWith("+86")) {
    if (!number.includes("+86")) {
      number = "+86" + number;
    }
    valid = true;
  }
  if (valid) {
    wx.makePhoneCall({
      phoneNumber: number
    })
  } else {
    wx.showModal({
      title: '提示',
      content: '只支持拨打澳洲或中国大陆的手机号码',
      showCancel: false,
    })
  }
}

function s_guid(s) {
  var p = (Math.random().toString(16) + "000000000").substr(2, 8);
  return s ? "-" + p.substr(0, 4) + "-" + p.substr(4, 4) : p;
}

function switchFav (propertyId) {
  let myFav = wx.getStorageSync("myFav");
  if (myFav) {
    let v = myFav[propertyId];
    myFav[propertyId] = !v;
  } else {
    myFav = {};
    myFav[propertyId] = true;
  }
  wx.setStorageSync("myFav", myFav);
  let propertyList = wx.getStorageSync("propertyList");
  let tmp = [];
  propertyList.forEach(p => {
    p.fav = myFav[p.id];
    tmp.push(p);
  })
  wx.setStorageSync("propertyList", tmp);
  wx.setStorageSync("refreshIndexPropertyList", true);
}

function isNumeric(n) {
  return !isNaN(parseFloat(n)) && isFinite(n);
}

/**------------------------------上传照片模块------------------------- */
/**
 * 文件名字
 * folder oss uploads下的文件夹名字
 * callBack  callback方法,设置上传成功后的操作
 */
function chooseImage(fileName, folder, callBack) {
  let self = this;
  checkAliOSSToken();
  wx.chooseImage({
    count: 1,
    sizeType: ['compressed'],
    success: function (res) {
      let tmpfpath = res.tempFilePaths[0];
      wx.getFileInfo({
        filePath: tmpfpath,
        success: function (res) {
          if (res.size / 1000 > 512) {
            wx.showToast({ title: '图片尺寸需小于512KB', icon: 'none', });
          } else {
            wx.showLoading({ title: '上传中...', mask: true });
            uploadingPic(wx.getStorageSync("ossToken"), tmpfpath, fileName, folder, callBack);
          }
        }
      });

    },
  })
}

/**------------------------------上传文件模块------------------------- */
/**
 * 文件名字
 * folder oss uploads下的文件夹名字
 * callBack  callback方法,设置上传成功后的操作
 */

function chooseFile(fileName, folder, callBack) {
  let self = this;
  checkAliOSSToken();
  wx.openDocument({
    success: function (res) {
      let tmpfpath = res.tempFilePaths[0];
      wx.getFileInfo({
        filePath: tmpfpath,
        success: function (res) {
          if (res.size / 1000 > 512) {
            wx.showToast({ title: '图片尺寸需小于512KB', icon: 'none', });
          } else {
            wx.showLoading({ title: '上传中...', mask: true });
            uploadingPic(wx.getStorageSync("ossToken"), tmpfpath, fileName, folder, callBack);
          }
        }
      });

    },
  })
}

function uploadingPic(ossToken, tmpfPath, fileName, folder, callBack) {
  let self = this;
  if (tmpfPath) {//上传图片
    wx.uploadFile({
      url: CON.IMAGE_URL,
      filePath: tmpfPath,
      name: 'file',
      formData: {
        'key': "uploads/" + folder + "/" + fileName,
        policy: ossToken['policy'],
        OSSAccessKeyId: ossToken['accessid'],
        success_action_status: "200",
        signature: ossToken['signature'],
      },
      success: function (res) {
        if (res.statusCode == 200) {
          wx.showToast({ title: '上传照片成功', });
          //self.setData({ picUrl1: filePath, fileName1: fileName });
          callBack(tmpfPath, fileName);
        } else {
          wx.showToast({ title: '上传照片失败,请稍后再试', icon: 'none', duration: 2000 });
        }
      },
      fail: function (res) {
        wx.showToast({
          title: '上传照片失败',
        })
      },
      complete: function (res) {
        wx.hideLoading();
      },
    })
  }
}

/**
 * aliosstoken获取
 */
function checkAliOSSToken() {
  let ossToken = wx.getStorageSync("ossToken");
  let now = Date.parse(new Date()) / 1000;
  if (!ossToken || ossToken['expire'] < now + 120) {
    NetUtil.netUtil(CON.ALI_OSS_URL, "GET", "", (ossToken) => {
      wx.setStorageSync("ossToken", ossToken);
    });
  }
}

module.exports = {
  formatTime: formatTime,
  makeCall: makeCall,
  s_guid: s_guid,
  switchFav: switchFav,
  isNumeric: isNumeric,
}
