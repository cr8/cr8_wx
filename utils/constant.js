//url相关
const BASE_URL = "https://api.cr8.hjaustralia.com/admin/api/rest"
//const BASE_URL = "http://localhost:3000/admin/api/rest"
const IMAGE_URL = "http://img.cr8.hjaustralia.com/";

const PROPERTY_URL = BASE_URL + "/properties/"
const GEO_URL = BASE_URL + "/api/property/geo/"
const ALI_OSS_URL = BASE_URL + "/api/property/alioss/";
const SMS_URL = BASE_URL + "/api/admin/";
const USER_URL = BASE_URL + "/api/user/";
const LANG_URL = BASE_URL + "/api/lang/";

const HEADERS = {
  'Merchant':'cr8',
  'content-type': 'application/json',
  'Authorization': 'Basic ZDY4MDA1NDppcXdLODI0IWMxMlI='
};

module.exports = {
  BASE_URL: BASE_URL,
  IMAGE_URL: IMAGE_URL,
  ALI_OSS_URL: ALI_OSS_URL,
  PROPERTY_URL: PROPERTY_URL,
  GEO_URL: GEO_URL,
  SMS_URL: SMS_URL,
  USER_URL: USER_URL,
  LANG_URL: LANG_URL,
  HEADERS: HEADERS,
}
