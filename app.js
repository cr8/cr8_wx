var CON = require('utils/constant.js');
var NET = require('utils/netUtil.js');
var LANG = require('utils/language.js');

//app.js
App({
  globalData: {
    registered: true,
    langs: null
  },

  onLaunch: function() {
    let self = this;
    wx.setStorageSync("registered", true);
    wx.getSystemInfo({
      success: function(res) {
        if (res.language == "en") {
          self.globalData.langs = LANG.english;
          wx.setTabBarItem({ index: 0, text: 'SEARCH', });
          wx.setTabBarItem({ index: 1, text: 'COLLECTIONS', });
          wx.setTabBarItem({ index: 2, text: 'ABOUT US', });
        } else {
          self.globalData.langs = LANG.chinese;
        }
      }
      
    })

  },

})